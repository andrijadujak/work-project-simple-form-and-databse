﻿namespace SimpleFormApi
{
    public class Osoba
    {
        private Guid id;
        private string ime;
        private string prezime;
        private string spol;
        private DateTime datum;
        private string adresa;
        private string telefon;

        public Osoba(string ime, string prezime, string spol, DateTime datum, string adresa, string telefon)
        {
            this.id = Guid.NewGuid();
            this.ime = ime;
            this.prezime = prezime;
            this.spol = spol;
            this.datum = datum;
            this.adresa = adresa;
            this.telefon = telefon;
        }

        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public string Spol { get => spol; set => spol = value; }
        public DateTime Datum { get => datum; set => datum = value; }
        public string Adresa { get => adresa; set => adresa = value; }
        public string Telefon { get => telefon; set => telefon = value; }
        public Guid Id { get => id; set => id = value; }

        public string getIdToString()
        {
            return id.ToString();
        }
    }
}
