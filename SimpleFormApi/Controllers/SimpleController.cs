﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using Newtonsoft.Json;
using SimpleFormApi;
using System.Diagnostics;

namespace SimpleFormApi.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class SimpleController : ControllerBase
    {
       
        private string filePath = @".\osobe.json";
        public SimpleController()
        {

        }



        private Dictionary<string, Osoba> deserialize()
        {
            var jsonData = System.IO.File.ReadAllText(filePath);
            if (jsonData != null)
            {
                return JsonConvert.DeserializeObject<Dictionary<string, Osoba>>(jsonData);
            }
            throw new System.Web.Http.HttpResponseException(HttpStatusCode.NotFound);
        }

        private void serialize(Dictionary<string, Osoba> osobe)
        {
            var jsonData = JsonConvert.SerializeObject(osobe);
            System.IO.File.WriteAllText(filePath, jsonData);
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        

        
        [HttpGet]
        public Osoba[] getOsobe()
        {
            if (!System.IO.File.Exists(filePath)){ initializeJson();}

            Dictionary<string, Osoba> osobaDN = deserialize();
            Osoba[] osobe= osobaDN.Values.ToArray();
            return osobe;
        }
        [HttpGet("{id}")]
        public Osoba getOsoba([FromRoute]string id)
        {
            
            var osobe = deserialize();
            if (!osobe.ContainsKey(id))
            {
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.NotFound);
            }
            return osobe[id];
        }

        public void initializeJson() 
        {
            string ime = "Andrija";
            string prezime = "Dujak";
            string spol = "Muški";
            DateTime datum = new DateTime(2010, 01, 01);
            string adresa = "zagrebacka 2";
            string telefon = "098745123";
            Osoba prvaOsoba = new Osoba(ime, prezime, spol, datum, adresa, telefon);
            Dictionary<string, Osoba> osobeDN = new Dictionary<string, Osoba>();
            osobeDN.Add(prvaOsoba.getIdToString(), prvaOsoba);
            var jsonData = JsonConvert.SerializeObject(osobeDN);
            System.IO.File.WriteAllText(filePath, jsonData);
        }

        [HttpPost]
        public void postNewOsoba(Osoba novaOsoba)
        {
            var osobe = deserialize();
            osobe.Add(novaOsoba.getIdToString(), novaOsoba);
            serialize(osobe);
        }

        [HttpPut]
        public void postEditOsoba(Osoba osoba)
        {
            var id = osoba.getIdToString();
            var osobe = deserialize();
            if (!osobe.ContainsKey(id))
            {
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.NotFound);
            }
            osobe[id] = osoba;
            serialize(osobe);
        }
    }
}


