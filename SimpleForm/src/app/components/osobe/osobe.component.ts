import { HttpClient } from '@angular/common/http';
import { Component, OnInit, SimpleChanges, OnChanges, SimpleChange,Input } from '@angular/core';
import { log } from 'console';
import { Osoba } from '../../models/Osoba'

@Component({
  selector: 'app-osobe',
  templateUrl: './osobe.component.html',
  styleUrls: ['./osobe.component.css']
})
export class OsobeComponent implements OnInit {
  osobe?: Osoba[];
  osobaEdit: Osoba;

  constructor(private http: HttpClient) {
    this.getTableOsobe();
    }

  private getTableOsobe() {
    this.http.get<Osoba[]>('/simple').subscribe(result => {
        console.log(result);
        this.osobe = result;
      }, error => console.log(error))
  }

  public editOsoba(osoba: Osoba) {
    this.http.get<Osoba>(`/simple/${osoba.id}`).subscribe(result => {
      this.osobaEdit = result;
    }, error => console.log(error));
  }

  public updateTable() {
    this.getTableOsobe();
  }
  ngOnInit(): void {
    
  }

}


