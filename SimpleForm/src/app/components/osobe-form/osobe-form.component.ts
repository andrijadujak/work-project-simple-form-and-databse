import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Osoba } from '../../models/Osoba'
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-osobe-form',
  templateUrl: './osobe-form.component.html',
  styleUrls: ['./osobe-form.component.css']
})
export class OsobeFormComponent implements OnInit {
  spolovi: string[] = ["Muški", "Ženski"];
  osobaFormControll: FormControl = new FormControl();
  osoba: Osoba = new Osoba();

  @Input()
  set osobaEdit(value: Osoba) {
    if (value) {
      var datum = new DatePipe('en-US').transform(value.datum, 'yyyy-MM-dd');
      this.osoba = value
      this.osoba.datum = datum || undefined;
    }
  }
  
  @Output() updateTable = new EventEmitter<null>();

  constructor(private http: HttpClient,) { }

  public onSubmit(): void {
    if (!this.osoba.id) { this.postOsoba(); }
    else { this.putOsoba(); }
    this.updateTable.emit()
  }

  private postOsoba(): void {
    this.http.post<Osoba>('/simple', this.osoba).subscribe(result => {
      console.log(result);
    }, error => console.log(error))
  }

  private putOsoba(): void {
    this.http.put('/simple', this.osoba).subscribe(result => {
      console.log(result);
    }, error => console.log(error))
  }

  ngOnInit(): void {
  }

}
