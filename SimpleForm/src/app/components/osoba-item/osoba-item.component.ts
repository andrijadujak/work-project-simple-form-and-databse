import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Osoba } from '../../models/Osoba';

@Component({
  selector: '[app-osoba-item]',
  templateUrl: './osoba-item.component.html',
  styleUrls: ['./osoba-item.component.css']
})
export class OsobaItemComponent implements OnInit {

  @Input()
  osoba:Osoba;

  @Output() editEvent = new EventEmitter<Osoba>();

  constructor() { }

  ngOnInit(): void {
  }

  onEdit(osoba: Osoba) {
    this.editEvent.emit(osoba);
  }
  
}
