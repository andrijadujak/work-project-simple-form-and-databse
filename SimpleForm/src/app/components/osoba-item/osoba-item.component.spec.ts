import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OsobaItemComponent } from './osoba-item.component';

describe('OsobaItemComponent', () => {
  let component: OsobaItemComponent;
  let fixture: ComponentFixture<OsobaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OsobaItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OsobaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
