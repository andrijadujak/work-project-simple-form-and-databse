import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { OsobeComponent } from './components/osobe/osobe.component'
import { OsobaItemComponent } from './components/osoba-item/osoba-item.component';
import { OsobeFormComponent } from './components/osobe-form/osobe-form.component'


@NgModule({
  declarations: [
    AppComponent,
    OsobeComponent,
    OsobaItemComponent,
    OsobeFormComponent  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
