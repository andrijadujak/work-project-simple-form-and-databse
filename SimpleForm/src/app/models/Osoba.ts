export class Osoba {
  id?: string;
  ime?: string;
  prezime?: string;
  spol?: string;
  datum?: string;
  adresa?: string;
  telefon?: string;
}
