const PROXY_CONFIG = [
  {
    context: [
      "/simple"
    ],
    target: "https://localhost:7247",
    secure: false
  }
]

module.exports = PROXY_CONFIG;
